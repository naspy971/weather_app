const find_city_weather_form = document.getElementById('find_city_weather_form');
const city_input = document.getElementById('city');
const selected_city_block = document.getElementById('selected_city_block');
const chosen_city_heading = document.getElementById('chosen_city_heading');
const search_temperature = document.getElementById('search_temperature');
const search_conditions = document.getElementById('search_conditions');
const search_wind_speed = document.getElementById('search_wind_speed');

// A la soumission du formulaire
find_city_weather_form.addEventListener('submit', e => {
        e.preventDefault();
        let chosenCity = city_input.value.trim();

        // Si l'input est correctement renseigné
        if (chosenCity !== '') {
            // Appel ajax pour récupérer la météo de la ville renseignée
            fetch('/weather/' + chosenCity)
                .then(response => response.json())
                .then(resJson => {
                    // Si la ville a été trouvée
                    if (!('error' in resJson)) {
                        chosen_city_heading.innerText = resJson.name;
                        search_conditions.innerText = resJson.weather[0].description;
                        search_temperature.innerText = resJson.main.temp;
                        search_wind_speed.innerText = (resJson.wind.speed * 3.6).toFixed(2);
                        city_input.value = '';
                        selected_city_block.style.display = 'block';
                    } else {
                        // Si la ville n'a pas été trouvée
                        selected_city_block.style.display = 'none';
                        chosen_city_heading.innerText = resJson.message;
                        city_input.value = '';
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        } else {
            alert('Veuillez saisir une ville svp !');
        }
    }
)
