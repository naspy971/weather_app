<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class WeatherService
{
    private $client;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->client = HttpClient::create();
        $this->apiKey = $apiKey;
    }

    /**
     * @return array|mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getToulouseWeather()
    {
        try {
            // Données météo de Toulouse
            $response = $this->client->request('GET', 'https://api.openweathermap.org/data/2.5/weather?lon=1.44&lat=43.6&lang=fr&units=metric&appid=' . $this->apiKey);
            return json_decode($response->getContent(), true);
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }


    /**
     * Retourne la météo de la ville choisie par l'utilisateur
     *
     * @param $city
     *
     * @return array|mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getWeatherByCity($city)
    {
        try {
            // Données météo de la ville sélectionnée par l'utilisateur
            $response = $this->client->request('GET', "https://api.openweathermap.org/data/2.5/weather?q=$city&lang=fr&units=metric&appid=" . $this->apiKey);
            return json_decode($response->getContent(), true);
        } catch (\Exception $e) {
            return ['error' => '404', 'message' => 'Ville non trouvée... :('];
        }
    }
}
