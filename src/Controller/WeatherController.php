<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\WeatherService;

class WeatherController extends AbstractController
{
    private $weatherService;

    public function __construct(WeatherService $weather)
    {
        $this->weatherService = $weather;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $toulouseWeather = $this->weatherService->getToulouseWeather();

        return $this->render('weather/index.html.twig', array(
            'toulouseWeather' => $toulouseWeather
        ));
    }

    /**
     * @Route("/weather/{city}", name="weather_by_city")
     */
    public function weatherByCity(string $city)
    {
        $cityWeather = $this->weatherService->getWeatherByCity($city);

        return new JsonResponse($cityWeather);
    }
}
